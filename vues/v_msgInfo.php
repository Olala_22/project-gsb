<?php
/**
 * Vue Erreurs
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>
<div class="container">
	<div class="alert alert-info" role="alert">
        <?php
        foreach ($_REQUEST['infos'] as $info) {
            echo '<p>' . htmlspecialchars($info) . '</p>';
        }
        ?>
	</div>
</div>