<?php
/**
 * Vue choix fiche de frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>
<div class="row">
	<div class="panel panel-info panel-info-comptable">
		<div class="panel-heading panel-heading-comptable ">Les fiche de frais <?php echo $lblEtat ?></div>

		<table
			class="table table-bordered table-responsive table-comptable table-bordered-comptable">
			<thead>
				<tr>
					<th>Visiteur</th>
					<th>DateModif</th>
					<th>Nb Justif.</th>
					<th>Montant</th>
					<th>Etat</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
                <?php
                foreach ($lesFichesFrais as $uneFicheFrais) {
                    
                    $nomVisiteur = $uneFicheFrais['nom'];
                    $idVisiteur = $uneFicheFrais['idVisiteur'];
                    $mois = $uneFicheFrais['mois'];
                    $dateModif = $uneFicheFrais['datemodif'];
                    $nbJustifs = $uneFicheFrais['nbjustificatifs'];
                    $montantValid = $uneFicheFrais['montantvalide'];
                    $lblEtat = $uneFicheFrais['libelle'];
                    ?>   
                    
                    <tr>
					<form method="post" role="form"
						action="index.php?uc=suiviPaiementFrais&action=resultFicheFraisComptable">
					
					
					<td> <?php echo $nomVisiteur ?>
                   		</td>
					<td> <?php echo $dateModif ?>
                        </td>
					<td> <?php echo $nbJustifs ?>
                   		</td>
					<td> <?php echo $montantValid ?>
                   		</td>
					<td> <?php echo $lblEtat ?>
                   		</td>
					<td>
						<button class="btn btn-success" type="submit" name="Voir">Voir</button>
						<input type="hidden" id="mois" name="mois"
						value="<?php echo $mois ?>"> <input type="hidden" id="idVisiteur"
						name="idVisiteur" value="<?php echo $idVisiteur ?>">
					</td>
					</form>
				</tr>
                    <?php
                }
                ?>
                </tbody>
		</table>

	</div>
</div>

