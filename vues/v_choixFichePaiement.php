<?php
/**
 * Gestion des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>
<div class="container">
	<form action="index.php?uc=suiviPaiementFrais&action=choixFicheEtat" 
              method="post" >
    	<div class="form-group">
    		<label for="lstEtat" accesskey="n">Choisir l'état de la fiche: </label>
            <select onchange='selectEtatFiche()' id="lstEtat" name="lstEtat" class="form-control field-display validMonthGroup">
                <option value="" ></option>
                <?php 
                foreach ($lesEtats as $unEtat) {
                    $lblEtat = $unEtat['lblfiche'];
                    $idEtat = $unEtat['idEtat'];
                    if (!$lesEtats == null) {
                        ?>
                        <option value= "<?php echo $idEtat;?>">
                        	<?php echo $lblEtat ?> 
                        </option>
                        <?php
                    }
                }
                ?> 
            </select>
            <script type="text/javascript">
				var selectEtatFiche = function() {
					if (document.getElementById("lstEtat").value != '') {
						window.location.href = 'index.php?uc=suiviPaiementFrais&action=afficheFicheChoixEtat&idEtat=' + document.getElementById("lstEtat").value ;
					}
				}
			</script>
        </div>
    </form>
</div>