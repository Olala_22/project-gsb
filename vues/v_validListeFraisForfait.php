<?php
/**
 * Vue Liste des frais au forfait
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte é Laboratoire GSB é
 */
?>
<div class="container">
<div class="row">    
    <h2 class="main-text">Valider la fiche de frais
    </h2>
    <h3>Eléments forfaitisés</h3>
    <div class="col-md-4 vue-bottom">
        <form method="post" 
              action="index.php?uc=validFrais&action=corrigerFrais" 
              role="form">
              <input type="hidden" id="visiteur" name="visiteur" value="<?php echo $idVisiteur ?>">
              <input type="hidden" id="mois" name="mois" value="<?php echo $mois ?>">
            <fieldset>       
                <?php
                foreach ($lesFraisForfait as $unFrais) {
                    $idFrais = $unFrais['idfrais'];
                    $libelle = htmlspecialchars($unFrais['libelle']);
                    $quantite = $unFrais['quantite']; ?>
                    <div class="form-group ">
                        <label for="idFrais"><?php echo $libelle ?></label>
                        <input type="text" id="idFrais" 
                               name="lesFrais[<?php echo $idFrais ?>]"
                                maxlength="100" 
                               value="<?php echo $quantite ?>" 
                               class="form-control control-size field-auto-size" size="15" >
                               
                    </div>
                    <?php 
                }
                ?>
                <button class="btn btn-success" type="submit"  >Corriger</button>
                <button class="btn btn-danger" type="reset">Réinitialiser</button>
            </fieldset>
        </form>
    </div>
</div>
</div>
