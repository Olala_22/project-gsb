<?php
/**
 * Vue Liste des frais hors forfait
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    R�seau CERTA <contact@reseaucerta.org>
 * @author    Jos� GIL <jgil@ac-nice.fr>
 * @copyright 2017 R�seau CERTA
 * @license   R�seau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte � Laboratoire GSB �
 */
?>

<div class="row">
    <div class="panel panel-info panel-info-comptable">
        <div class="panel-heading panel-heading-comptable ">Descriptif des éléments hors forfait</div>
        <script type="text/javascript">
        function refus() {
            alert("les frais hors forfais ont bien été mis en 'refus'");
        }</script>
            <table class="table table-bordered table-responsive table-comptable table-bordered-comptable">
                <thead>
                    <tr>
                        <th >Date</th>
                        <th >Libellé</th>  
                        <th >Montant</th>  
                        <th >&nbsp;</th> 
                    </tr>
                </thead>  
                <tbody>
                <?php
                foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                    $libelle = htmlspecialchars($unFraisHorsForfait['libelle']);
                    $date = $unFraisHorsForfait['date'];
                    $montant = $unFraisHorsForfait['montant'];
                    $id = $unFraisHorsForfait['id']; ?>    
                    <tr>
                    	<td> <input type="text" id="date" 
                                    name="date"
                                    maxlength="100" 
                                   value="<?php echo $date ?>" 
                                   class="form-control control-size field-auto-size" size="15" >
                   		</td>
                   		<?php
                   		if(isset($_POST['supprimer'])) { ?>
                            <td>
                            <input type="text" id="libelle"
                                 name="libelle"
                                 maxlength="100"
                                 value="<?php echo $libelle + "REFUSE" ?>"
                                 class="form-control control-size field-auto-size" size="15" >
     						</td>
                        <?php } else { ?>
                            <td>
                            <input type="text" id="libelle"
                                 name="libelle"
                                 maxlength="100"
                                 value="<?php echo $libelle ?>"
                                 class="form-control control-size field-auto-size" size="15" >
                             </td>
                        <?php } ?>
                        <td> <input type="text" id="montant" 
                                   name="montant"
                                    maxlength="100" 
                                   value="<?php echo $montant ?>" 
                                   class="form-control control-size field-auto-size" size="15" >
                   		</td>
                        <td> 
                        <form method="post" action="index.php?uc=validFrais&action=corrigerFrais" >
                            <button class="btn btn-success" type="submit" name="corriger" >Corriger</button>
                        </form>
                        <form method="post" action="index.php?uc=validFrais&action=" >
                			<button class="btn btn-danger" type="reset" >Réinitialiser</button>
                		</form>	
                        <form method="post" action="index.php?uc=validFrais&action=supprimerFraisHorsForfait" >
                			<button class="btn btn-warning" type="submit" name="supprimer" onclick="refus();">Supprimer</button>
            				<input type="hidden" id="idFrais" name="idFrais" value="<?php echo $id ?>">  
                		</form>	
                		</td>
                    </tr>
                <?php }
                ?>
                </tbody>  
            </table>
    </div>
</div>


