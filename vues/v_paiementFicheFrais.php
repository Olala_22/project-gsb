<?php
/**
 * vue validation etat frais 
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>

<hr>
<div class="panel panel-info panel-info-comptable">
<div class="panel-heading panel-heading-comptable ">Fiche de frais du mois
<?php echo $mois ?> : </div>
    <div class="panel-body">
        <strong><u>Etat :</u></strong> <?php echo $lblEtat ?>
        depuis le: <?php echo $dateModif ?> <br> 
        <strong><u>Montant validé :</u></strong> <?php echo $montantValid ?>
    </div>
</div>
<div class="panel panel-info panel-info-comptable">
    <div class="panel-heading panel-heading-comptable ">Eléments forfaitisés</div>
    <table class="table table-bordered table-responsive">
        <tr>
            <?php
            foreach ($lesFraisForfait as $unFraisForfait) {
                $libelle = $unFraisForfait['libelle']; ?>
                <th> <?php echo htmlspecialchars($libelle) ?></th>
                <?php
            }
            ?>
        </tr>
        <tr>
            <?php
            foreach ($lesFraisForfait as $unFraisForfait) {
                $quantite = $unFraisForfait['quantite']; ?>
                <td class="qteForfait"><?php echo $quantite ?> </td>
                <?php
            }
            ?>
        </tr>
    </table>
</div>
<?php if ($lesFraisHorsForfait != null){?>
    <div class="panel panel-info panel-info-comptable">
        <div class="panel-heading panel-heading-comptable ">Descriptif des éléments hors forfait - 
            <?php echo $nbJustificatifs ?> justificatifs reçus</div>
        <table class="table table-bordered table-responsive">
            <tr>
                <th class="date">Date</th>
                <th class="libelle">Libellé</th>
                <th class='montant'>Montant</th>                
            </tr>
            <?php
            foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                $date = $unFraisHorsForfait['date'];
                $libelle = htmlspecialchars($unFraisHorsForfait['libelle']);
                $montant = $unFraisHorsForfait['montant']; ?>
                <tr>
                    <td><?php echo $date ?></td>
                    <td><?php echo $libelle ?></td>
                    <td><?php echo $montant ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
<?php }?>
<div >
	<form action="index.php?uc=suiviPaiementFrais&action=ficheEtatRB">
		<button class="btn btn-success" type="submit" name="etatRB">Mettre en paiement</button>
	</form>
</div>

