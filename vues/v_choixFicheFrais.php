<?php
/**
 * Vue choix fiche de frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>
<div class="container">
    	<form action="index.php?uc=validFrais&action=resultFicheFrais" 
                  method="post" >
        	<div class="form-group">
        		<label for="lstVisiteur" accesskey="n">Choisir le visiteur: </label>
                <select onchange='selectVistorMois()' id="lstV" name="lstVisiteur" class="form-control field-display validMonthGroup">
                    <?php 
                    foreach ($lesVisiteurs as $unVisiteur) {
                        $nom = $unVisiteur['nom'];
                        $id = $unVisiteur['id'];
                        $prenom = $unVisiteur['prenom'];
                        if (!$lesVisiteurs == null) {
                            ?>
                            <option value= "<?php echo $id;  ?>" <?php if ($visiteurASelectionner == $id) {echo 'selected';}?>>
                            	<?php echo $nom . ' ' . $prenom ?> 
                            </option>
                            <?php
                        }
                    }
                    ?> 
                </select>
            	<label for="lblMois" accesskey="n">Mois : </label>
                <select  onchange='selectCorrigeMois()' id="lstMois" name="lstMois" class="form-control field-display validMonthGroup">
                    <option value="" ></option>
                    <?php
                    foreach ($lesMois as $unMois) {
                        $leMois = $unMois['mois'];
                        $numAnnee = $unMois['numAnnee'];
                        $nbMois = $unMois['numMois'];
                        ?>
                        <option value="<?php echo $leMois; ?>" <?php if ($moisASelectionner == $leMois) {echo 'selected';}?>>
                            <?php echo $nbMois . '/' . $numAnnee ?> 
                        </option>
                        <?php
                    }
                    ?>    
                </select>
                <script type="text/javascript">
					var selectVistorMois = function() {
						window.location.href = 'index.php?uc=validFrais&action=selectFicheVisiteur&visiteur=' + document.getElementById("lstV").value 
						+'&mois='+ document.getElementById("lstMois").value ;
					}
					var selectCorrigeMois = function() {
						if (document.getElementById("lstMois").value != '') {
    						window.location.href = 'index.php?uc=validFrais&action=resultFicheFrais&visiteur=' + document.getElementById("lstV").value 
    						+'&mois='+ document.getElementById("lstMois").value ;
						}
					}
				</script>
            </div>
        </form>
</div>
