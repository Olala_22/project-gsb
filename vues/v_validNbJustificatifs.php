<?php
/**
 * Vue Entête
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>

<div class="row">    
			
    <div class="col-md-4 vue-bottom">
    	
        <form method="post" action="index.php?uc=validFrais&action=validFicheFrais" role="form">
        	<input type="hidden" id="visiteur" name="visiteur" value="<?php echo $idVisiteur ?>">
            <input type="hidden" id="mois" name="mois" value="<?php echo $mois ?>">
              <fieldset> 
              	<div class="form-group ">				
                  <label for="idNbjustificatifs">Nombre de justificatifs:</label>
                  <input type="text" id="idNbjustificatifs" 
                       name="idNbjustificatifs"maxlength="5" 
                       value="<?php echo $lesNbJustificatifs ?>" 
                       class="form-control control-size ield-small-size field-display field-size-justif" >
              	</div>
              </fieldset> 
              <div class="form-group">
                  <button class="btn btn-success" type="submit">Valider</button>
                  <button class="btn btn-danger" type="reset">Réinitialiser</button>
              </div>
        </form>
    </div>
</div>