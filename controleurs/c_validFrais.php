<?php
/**
 * Gestion des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */


//$idVisiteur = $_SESSION['idVisiteur'];
$moisSession = getMois(date('d/m/Y'));
$numAnnee = substr($moisSession, 0, 4);
$numMois = substr($moisSession, 4, 2);
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$idVisiteur = filter_input(INPUT_GET, 'visiteur', FILTER_SANITIZE_STRING);
$mois = filter_input(INPUT_GET, 'mois', FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);

if (empty($idVisiteur)) {
    $idVisiteur = filter_input(INPUT_POST, 'visiteur', FILTER_SANITIZE_STRING);
}
if (empty($mois)) {
    $mois = filter_input(INPUT_POST, 'mois', FILTER_SANITIZE_STRING);
}

//récuperation des visiteurs
$lesVisiteurs = $pdo->getLesVisiteurs();
$lesCles = array_keys($lesVisiteurs);

if(empty($idVisiteur)) {
    $idVisiteur = $lesVisiteurs[0]["id"];
}
// récuperation des mois
$lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
$lesClesMois = array_keys($lesMois);
if(empty($mois)) {
    $mois = $lesClesMois[0]["mois"];
}

$visiteurASelectionner = $idVisiteur;
$moisASelectionner = $mois;
switch ($action) {
    case 'selectFicheVisiteur':
        include 'vues/v_choixFicheFrais.php';
        if(count($lesClesMois) == 0 ) {
            ajouterErreur('Pas de fiche de frais pour ce visiteur ce mois');
            include 'vues/v_erreurs.php';
        }
        
        break; 
    case 'resultFicheFrais':
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
            $lesNbJustificatifs = $pdo->getNbjustificatifs($idVisiteur, $mois);
            include 'vues/v_choixFicheFrais.php';
            include 'vues/v_validListeFraisForfait.php';
            if ($lesFraisHorsForfait != null){
                include 'vues/v_validListeFraisHorsFrais.php';
            } else {
                ajouterErreur("Pas d'éléments hors forfait pour le visiteur ce mois");
                include 'vues/v_erreurs.php';
            }
            include 'vues/v_validNbJustificatifs.php';
        break;
    // TODO faire retourner les éléments qui ont été modifier
    case 'corrigerFrais':
            $lesFrais = filter_input(INPUT_POST, 'lesFrais', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
            $pdo->majFraisForfait($idVisiteur, $mois, $lesFrais);
            $pdo->majFraisHorsForfait($idVisiteur, $mois);
            ajouterMsgInfo('la modification a bien été prise en compte');
            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
            $lesNbJustificatifs = $pdo->getNbjustificatifs($idVisiteur, $mois);
            include 'vues/v_choixFicheFrais.php';
            include 'vues/v_validListeFraisForfait.php';
            include 'vues/v_msgInfo.php';
            include 'vues/v_validListeFraisHorsFrais.php';
            include 'vues/v_validNbJustificatifs.php';
        break;
    case'supprimerFraisHorsForfait':
        $idFrais = filter_input(INPUT_POST, 'idFrais', FILTER_SANITIZE_STRING);
        echo 'aaaaaaaaaaaaaaa' . $idFrais;
        $pdo->refusFraisHorsForfait($idFrais);
        /*$lesFrais = filter_input(INPUT_POST, 'lesFrais', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
        $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);*/
        /*include 'vues/v_choixFicheFrais.php';
        include 'vues/v_validListeFraisForfait.php';
        include 'vues/v_validListeFraisHorsFrais.php';
        ajouterMsgInfo('la modification a bien été prise en compte des éléments suivant;
                Frais hors forfait:' .$idFrais .'à bien été supprimer');
        include 'vues/v_validNbJustificatifs.php';*/
        break;
    case'validFicheFrais':
        $etat = "VA";
        $pdo->majEtatFicheFrais($idVisiteur, $mois, $etat);
        ajouterMsgInfo('la fiche a bien été valider ' . $idVisiteur ." " .$mois);
        include 'vues/v_validFrais.php';
        include 'vues/v_msgInfo.php';
        break;
    
} 
