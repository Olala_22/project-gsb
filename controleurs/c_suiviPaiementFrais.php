<?php
/**
 * Gestion des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    khalifa
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */

$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
switch ($action) {
    case 'afficheFicheChoixEtat':
        $lblEtat = filter_input(INPUT_GET, 'libEtat', FILTER_SANITIZE_STRING);
        $lesEtats = $pdo->getLesIEtats();
        $etat = "VA";
        $lesFichesFrais = $pdo->getLesInfosFicheFraisEtat($etat);
        if(!$lesFichesFrais == null){
            include 'vues/v_choixComptableFicheFrais.php';
        } else {
            ajouterErreur('Pas de fiche de frais à valider et mise en paiement !');
            include 'vues/v_erreurs.php';
        }
        break;
    case'resultFicheFraisComptable':
        $lesEtats = $pdo->getLesIEtats();
        $lblEtat = filter_input(INPUT_GET, 'libEtat', FILTER_SANITIZE_STRING);
        $idVisiteur = filter_input(INPUT_POST, 'idVisiteur', FILTER_SANITIZE_STRING);
        $mois = filter_input(INPUT_POST, 'mois', FILTER_SANITIZE_STRING);
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
        $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $mois);
        //$lesFichesFrais = $pdo->getLesInfosFicheFraisEtat($etat);
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $lblEtat = $lesInfosFicheFrais['libEtat'];
        $dateModif = $lesInfosFicheFrais['dateModif'];
        $montantValid = $lesInfosFicheFrais['montantValide'];
        if($lesFraisHorsForfait == null){
            ajouterErreur('Pas de frais hors forfait pour cette fiche !');
            include 'vues/v_erreurs.php';
        } 
        include 'vues/v_paiementFicheFrais.php';
        break;
    case 'ficheEtatRB':
        $etatRB = "RB";
        $pdo->majEtatFicheFrais($idVisiteur, $mois, $etatRB);
        ajouterMsgInfo('Le remboursement à été effectué !');
        include 'vues/v_paiementFicheFrais.php';
        include 'vues/v_msgInfo.php';
        break;
        
}
